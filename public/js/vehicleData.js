const HOST = window.location.origin;

function getVehicles() {
  fetch(HOST + '/vehicleData/getVehicles').then((response) => {
    if(response.ok) return response = response.json();
  }).then((data ) => {
    console.log('vehicles', data);
  }).catch(error => console.log(error));

}

async function getGps() {
  try{
    var response = await fetch(HOST + '/vehicleData/getGps');
    var gps =  await response.json();
    console.log('gps', gps);
    return gps;
  
  }catch(error){
      console.log(error);
  }
 
}

async function getGpsByVehicle() {
  var vehicleName = document.getElementById('getGpsByVehicleText').value;
  let gpsVehicleSpecific = await getGps();

  console.log(gpsVehicleSpecific);

  gpsVehicleSpecific.data.filter((val)=> {
    return val.data.vehicleName === vehicleName;
  })

  console.log('gpsVehicleSpecific', gpsVehicleSpecific);
}
